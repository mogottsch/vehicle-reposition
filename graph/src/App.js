import CytoscapeComponent from "react-cytoscapejs";
import graph from "./graph"
function App() {  
  return (
    <CytoscapeComponent
      elements={CytoscapeComponent.normalizeElements(graph)}
      style={{ width: "2000px", height: "1500px" }}
      layout={{
        name: 'preset'
      }}
      stylesheet={[
        {
          selector: 'node',
          style: {
            label: "data(id)",
            width: "data(size)",
            height: "data(size)",
          }
        },
        {
          selector: 'edge',
          style: {
            width: 1,
            // 'mid-target-arrow-shape': 'triangle',
            "curve-style": "bezier",
          }
        },
        {
          selector: 'edge[type = "relocation"]',
          style: {
            'line-color': 'red',
            width: 'data(size)',
          }
        },
        {
          selector: 'edge[type = "trip"]',
          style: {
            'line-color': 'blue',
            width: 'data(size)',
          }
        },
      ]}   
    />
  );
}

export default App;
